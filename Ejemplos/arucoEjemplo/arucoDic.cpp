#include <opencv2/aruco.hpp>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <sstream>
#include <iostream>

using namespace std;
using namespace cv;

//Matrices de laptop, realizar calibracion y reemplazar con TX1
const Mat cameraMatrix = (Mat_<double>(3,3) << 

    1.4149463861018060e+03, 0.0, 9.6976370017096372e+02, 
    0.0, 1.4149463861018060e+03, 5.3821002771506880e+02,
    0.0, 0.0, 1.0

);

const Mat distCoeffs = (Mat_<double>(5,1) << 1.8580734579482813e-01, -5.5388292695096419e-01, 1.9639104707396063e-03, 4.5272274621161552e-03, 5.3671862979121965e-01
);

//Funcion necesaria para el videoCapture
std::string get_tegra_pipeline(int width, int height, int fps) {
    return "nvcamerasrc ! video/x-raw(memory:NVMM), width=(int)" + std::to_string(width) + ", height=(int)" +
           std::to_string(height) + ", format=(string)I420, framerate=(fraction)" + std::to_string(fps) +
           "/1 ! nvvidconv flip-method=0 ! video/x-raw, format=(string)BGRx ! videoconvert ! video/x-raw, format=(string)BGR ! appsink";
}
/*************** Salida del pipeline(***************
Using pipeline: 
	nvcamerasrc ! video/x-raw(memory:NVMM), width=(int)1280, height=(int)720, format=(string)I420, framerate=(fraction)30/1 ! nvvidconv flip-method=0 ! video/x-raw, format=(string)BGRx ! videoconvert ! video/x-raw, format=(string)BGR ! appsink

Available Sensor modes : 
2592 x 1944 FR=30.000000 CF=0x1109208a10 SensorModeType=4 CSIPixelBitDepth=10 DynPixelBitDepth=10
2592 x 1458 FR=30.000000 CF=0x1109208a10 SensorModeType=4 CSIPixelBitDepth=10 DynPixelBitDepth=10
1280 x 720 FR=120.000000 CF=0x1109208a10 SensorModeType=4 CSIPixelBitDepth=10 DynPixelBitDepth=10

NvCameraSrc: Trying To Set Default Camera Resolution. Selected sensorModeIndex = 1 WxH = 2592x1458 FrameRate = 30.000000
*/


// Funcion usada para crear los marcadores de la carpeta 'markers'
// Se seleccionar un diccionario de los por defecto de la libreria 
// y tomamos solo los primeros 6 para pruebas
bool createMarker(const string name, const int size){
    Mat markerImage;
    Ptr<aruco::Dictionary> dictionary = aruco::getPredefinedDictionary(aruco::DICT_6X6_250);
    for(int i = 0; i < size; ++i){
        aruco::drawMarker(dictionary, i, 200, markerImage, 1);//solo toma los primeros <size> marcadores por ahora
        stringstream ss;
        ss << name << i << ".png";
        imwrite(ss.str(), markerImage);
    }
    return true;
}

bool detectMarkers(){
	    // Options
    //int WIDTH = 1920; // Con 1080 parece alentarse el despliegue
    //int HEIGHT = 1080;
    int WIDTH = 1280; // Con 720 se ve decente (un poco lento). Cada uso de los 4 CPU cerca del 80%, 
    int HEIGHT = 720;
    //int WIDTH = 640; // Con 480 alcanza los 60 fps sin problemas
    //int HEIGHT = 480;
    int FPS = 30;
	std::string pipeline = get_tegra_pipeline(WIDTH, HEIGHT, FPS);
    std::cout << "Using pipeline: \n\t" << pipeline << "\n";
    int waitTime = 30;
    VideoCapture inputVideo;
    inputVideo.open(pipeline, CAP_GSTREAMER);
    if (!inputVideo.isOpened()) {
        std::cout << "Connection failed";
        return -1;
    }
    Ptr<aruco::Dictionary> dictionary = aruco::getPredefinedDictionary(aruco::DICT_6X6_250);
    /*while (inputVideo.grab()) { // Probamos solo despliegue
        Mat image, imageCopy;
        inputVideo.retrieve(image);
		imshow("out", image);
		waitKey(1);
	}
	return true;*/
    while (inputVideo.grab()) {
        Mat image, imageCopy;
        inputVideo.retrieve(image);
        image.copyTo(imageCopy);
        vector<int> ids;
        vector< vector<Point2f> > corners;
        aruco::detectMarkers(image, dictionary, corners, ids);
        // al menos un marcador detectado
        if (ids.size() > 0) {
            aruco::drawDetectedMarkers(imageCopy, corners, ids);
            vector<Vec3d> rvecs, tvecs;
            aruco::estimatePoseSingleMarkers(corners, 0.05, cameraMatrix, distCoeffs, rvecs, tvecs);
            // dibujo de los ejes de los marcadores
            for(unsigned int i = 0; i < ids.size(); ++i)
                aruco::drawAxis(imageCopy, cameraMatrix, distCoeffs, rvecs[i], tvecs[i], 0.1);
        }
        imshow("out", imageCopy);
        char key = (char) waitKey(1);// ya le pasamos los fps al pipeline
        if (key == 27)
            break;
    }
    return true;
}

int main(){
    cout << "Hello" << endl;
    //createMarker("markers/test", 6);
    detectMarkers();
    return 0;
}
