#include <opencv2/aruco.hpp>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <sstream>
#include <fstream>
#include <string>
#include <iostream>
#define CAPTURE_DELAY 60 //frames de temporizador para capturar imagen

using namespace std;
using namespace cv;

int main(){
	// Options
    int WIDTH = 1920; // La mayor resolucion para calibracion
    int HEIGHT = 1080;
	Size dispSize(1280, 720); //resize para el despliegue porque no pude minimizar ventana
    int FPS = 30;
	//std::string pipeline = get_tegra_pipeline(WIDTH, HEIGHT, FPS);
    //std::cout << "Using pipeline: \n\t" << pipeline << "\n";
    int waitTime = 30;
    VideoCapture inputVideo;
    //inputVideo.open(pipeline, CAP_GSTREAMER);
	inputVideo.open("salida.mvk");
	bool captureInProc = false;
	int framesUntilCap = CAPTURE_DELAY;
	int framesCaptured = 0;
	string dir = "calib2/captura";
    if (!inputVideo.isOpened()) {
        std::cout << "Connection failed";
        return -1;
    }
	ifstream inFile("../arucoEjemplo/vecsData.txt");
	string line;
    while (inputVideo.grab()) {
        Mat image, resImage;
        inputVideo.retrieve(image);
		if(getline(inFile, line)){
			cout << line << endl;
		}
		resize(image, resImage, dispSize);
        imshow("out", resImage);
        char key = (char) waitKey();// ya le pasamos los fps al pipeline
        if (key == 27)
            break;
		if (key == 99){
			captureInProc = true;
		}
    }
    return 1;
}
