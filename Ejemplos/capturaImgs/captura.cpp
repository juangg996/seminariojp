#include <opencv2/aruco.hpp>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <sstream>
#include <iostream>
#define CAPTURE_DELAY 60 //frames de temporizador para capturar imagen

using namespace std;
using namespace cv;

//Funcion necesaria para el videoCapture
std::string get_tegra_pipeline(int width, int height, int fps) {
    return "nvcamerasrc ! video/x-raw(memory:NVMM), width=(int)" + std::to_string(width) + ", height=(int)" +
           std::to_string(height) + ", format=(string)I420, framerate=(fraction)" + std::to_string(fps) +
           "/1 ! nvvidconv flip-method=0 ! video/x-raw, format=(string)BGRx ! videoconvert ! video/x-raw, format=(string)BGR ! appsink";
}

int main(){
	// Options
    int WIDTH = 1920; // La mayor resolucion para calibracion
    int HEIGHT = 1080;
	Size dispSize(1280, 720); //resize para el despliegue porque no pude minimizar ventana
    int FPS = 30;
	std::string pipeline = get_tegra_pipeline(WIDTH, HEIGHT, FPS);
    std::cout << "Using pipeline: \n\t" << pipeline << "\n";
    int waitTime = 30;
    VideoCapture inputVideo;
    inputVideo.open(pipeline, CAP_GSTREAMER);
	bool captureInProc = false;
	int framesUntilCap = CAPTURE_DELAY;
	int framesCaptured = 0;
	string dir = "calib2/captura";
    if (!inputVideo.isOpened()) {
        std::cout << "Connection failed";
        return -1;
    }
    while (inputVideo.grab()) {
        Mat image, resImage;
        inputVideo.retrieve(image);
		if(captureInProc){// inicia conteo para captura
			framesUntilCap--;
			if(!framesUntilCap){
				framesUntilCap = CAPTURE_DELAY;
				captureInProc = false;
		        stringstream ss;
		        ss << dir << framesCaptured++ << ".png";
		        imwrite(ss.str(), image);//captura
			}
			stringstream ss2;
			ss2 << "Captura en " << framesUntilCap;
			putText(image, ss2.str(), Point(50,50), 1, 4, Scalar(20,20,200), 3 );
		}
		resize(image, resImage, dispSize);
        imshow("out", resImage);
        char key = (char) waitKey(1);// ya le pasamos los fps al pipeline
        if (key == 27)
            break;
		if (key == 99){
			captureInProc = true;
		}
    }
    return 1;
}
